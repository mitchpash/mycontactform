﻿using System;
using System.Data.SqlClient; //Using SQL

namespace MyContactForm.Models
{
    public class ContactFormDataAccess
    {
        public static bool InsertContactForm(ContactForm contactForm)
        {
            using (
                SqlConnection con =
                    new SqlConnection(
                        System.Configuration.ConfigurationManager.ConnectionStrings["HostedConnection"]
                            .ConnectionString))
            {
                //create
                con.Open(); //opens a new connection to the database
                try //use
                {
                    //SQL Call
                    SqlCommand command =  new SqlCommand("INSERT INTO ContactForms (Name, Email, Body) Values (@name, @email, @body", con);
                    //Avoids nasty SQL injections
                    command.Parameters.Add(new SqlParameter("name", contactForm.Name));
                    command.Parameters.Add(new SqlParameter("email", contactForm.Email));
                    command.Parameters.Add(new SqlParameter("body", contactForm.Body));
                    //Run
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            } //destroy
        }
    }
}