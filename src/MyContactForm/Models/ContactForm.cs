﻿using System;

namespace MyContactForm.Models
{
    public class ContactForm
    {

        public int ContactFormId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Body { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CratedAt { get; set; }

        public ContactForm()
        {
            
        }
        public ContactForm(int contactFormId, string name, string email, string body, DateTime updatedAt, DateTime cratedAt)
        {
            ContactFormId = contactFormId;
            Name = name;
            Email = email;
            Body = body;
            UpdatedAt = updatedAt;
            CratedAt = cratedAt;
        }

    }
}